#!/bin/bash

os=$(lsb_release -i -s)
version=$(curl https://download.samba.org/pub/samba/stable/ -s | grep -e 'samba-[0-9].[0-9].[0-9].tar.gz' | sed -r 's/^.*href="samba-([0-9].[0-9].[0-9]).tar.gz".*$/\1/' | tail -n1)
echo "latest version: $version"

add_signature() {
    echo "downloading gpg key"
    curl "https://download.samba.org/pub/samba/samba-pubkey.asc" -o samba-pubkey.asc -s
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to download gpg key"; exit 1; fi
    gpg --import samba-pubkey.asc
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to add gpg key"; exit 2; fi
    echo "add_signature" > installation.swp
}

verify_package() {
    echo "verifying package"
    curl "https://download.samba.org/pub/samba/stable/samba-$version.tar.asc" -o samba-$version.tar.asc -s
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to download signature file"; exit 1; fi
    gpg --verify samba-$version.tar.asc
    if [[ $? -ne 0 ]]; then echo "$LINENO: BAD SIGNATURE! check package file"; exit 2; fi
    echo "verify_package" > installation.swp
}

download_package() {
    echo "downloading package"
    curl "https://download.samba.org/pub/samba/stable/samba-$version.tar.gz" -o samba-$version.tar.gz
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to download package"; exit 1; fi
    echo "download_package" > installation.swp
}

extract_package() {
    echo "extracting package"
    gunzip samba-$version.tar.gz
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to extract package"; exit 1; fi
    echo "extract_package" > installation.swp
}

create_service() {
    echo "creating service"
    # smb.conf file MUST be created to make smbd.service work
    #echo -n "enter share folder full path: "
    #read SHARE_PATH
    #sed -i "s/path = .*$/path = $SHARE_PATH/g" smb.conf
    sudo cp /usr/local/samba/etc/smb.conf
    if [[ -d /etc/systemd/system ]]; then
        sudo cp smbd.service /etc/systemd/system/
    else
        echo "/etc/systemd/system/ directory does not exist, creating it"
        sudo mkdir /etc/systemd/system
        sudo cp smbd.service /etc/systemd/system/
    fi
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to create service"; exit 1; fi
    export PATH=$PATH:/usr/local/samba/bin
    export PATH=$PATH:/usr/local/samba/sbin
    echo "create_service" > installation.swp
}

configure_package() {
    echo "configuring package"
    tar -xf samba-$version.tar
    cd samba-$version
    ./configure
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to configure package"; exit 1; fi
    cd ..
    echo "configure_package" > installation.swp
}

compile_package() {
    echo "compiling package"
    cd samba-$version
    make -j$(nproc)
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to compile package"; exit 1; fi
    cd ..
    echo "compile_package" > installation.swp
}

install_package() {
    echo "installing package"
    cd samba-$version
    sudo make -j$(nproc) install
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to install package"; exit 1; fi
    cd ..
    echo "install_package" > installation.swp
}

dependencies() {
    if [[ $1 == "Debian" ]] || [[ $1 == "Ubuntu" ]]; then
        sudo apt install acl attr autoconf bind9utils bison build-essential \
        debhelper dnsutils docbook-xml docbook-xsl flex gdb libjansson-dev krb5-user \
        libacl1-dev libaio-dev libarchive-dev libattr1-dev libblkid-dev libbsd-dev \
        libcap-dev libcups2-dev libgnutls28-dev libgpgme11-dev libjson-perl \
        libldap2-dev libncurses5-dev libpam0g-dev libparse-yapp-perl \
        libpopt-dev libreadline-dev nettle-dev perl perl-modules pkg-config \
        python-all-dev python-crypto python-dbg python-dev python-dnspython \
        python3-dnspython python-gpgme python3-gpgme python-markdown python3-markdown \
        python3-dev xsltproc zlib1g-dev liblmdb-dev lmdb-utils -o Acquire::ForceIPv4=true
    elif [[ $1 == "Redhat" ]] || [[ $1 == "Centos" ]]; then
        sudo yum install attr bind-utils docbook-style-xsl gcc gdb krb5-workstation \
        libsemanage-python libxslt perl perl-ExtUtils-MakeMaker \
        perl-Parse-Yapp perl-Test-Base pkgconfig policycoreutils-python \
        python2-crypto gnutls-devel libattr-devel keyutils-libs-devel \
        libacl-devel libaio-devel libblkid-devel libxml2-devel openldap-devel \
        pam-devel popt-devel python-devel readline-devel zlib-devel systemd-devel \
        lmdb-devel jansson-devel gpgme-devel pygpgme libarchive-devel
    fi
    if [[ $? -ne 0 ]]; then echo "$LINENO: failed to install dependencies"; exit 4; fi
    echo "dependencies" > installation.swp
}

################################################################################

if ! [[ -f installation.swp ]]; then
    add_signature
    download_package
    extract_package
    verify_package
    dependencies $os
    compile_package
    install_package
    create_service
else
    if test $(cat installation.swp) == "add_signature"; then
        download_package
        extract_package
        verify_package
        dependencies $os
        configure_package
        compile_package
        install_package
        create_service
    elif test $(cat installation.swp) == "download_package"; then
        extract_package
        verify_package
        dependencies $os
        configure_package
        compile_package
        install_package
        create_service
    elif test $(cat installation.swp) == "extract_package"; then
        verify_package
        dependencies $os
        configure_package
        compile_package
        install_package
        create_service
    elif test $(cat installation.swp) == "verify_package"; then
        dependencies $os
        configure_package
        compile_package
        install_package
        create_service
    elif test $(cat installation.swp) == "dependencies"; then
        configure_package
        compile_package
        install_package
        create_service
    elif test $(cat installation.swp) == "configure_package"; then
        compile_package
        install_package
        create_service
    elif test $(cat installation.swp) == "compile_package"; then
        install_package
        create_service
    elif test $(cat installation.swp) == "install_package"; then
        create_service
    elif test $(cat installation.swp) == "create_service"; then
        echo "nothing left to do"
    else
        echo "remove installation.swp file and try again"
    fi
fi
