all:
	@echo "nothing to install"

server: manager
	./manager server

client: manager
	./manager client

clean:
	rm -rf *.tar *.gz *.asc *.swp
	rm -rf samba-*
