# Bonding Project

Auto-configuring samba project so that two Linux systems can connect through multi-channel.

## installation

    ./configure

## Running

only run these commands after complete installation!

### Server

    make server

### Client

**Note:** making a client in the same system that server is running is none sense!

    make client
